import java.io.*;
import java.net.*;

public class VelhaClient {
    public static void main(String[] args) {
        // Configuração do servidor: endereço e porta
        String serverAddress = "localhost"; // Endereço do servidor
        int serverPort = 12345; // Porta do servidor

        try {
            // Cria um socket para se conectar ao servidor
            Socket socket = new Socket(serverAddress, serverPort);

            // Cria leitores para entrada e saída de dados
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // Lê a mensagem inicial do servidor
            String message = in.readLine();
            System.out.println(message);

            // Prepara para ler as entradas do usuário
            BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
            String serverMessage;

            while (true) {
                // Lê mensagens do servidor
                serverMessage = in.readLine();

                // Verifica se a conexão foi encerrada pelo servidor
                if (serverMessage == null) {
                    break;
                }

                // Encerra o loop se o servidor enviar "Fim do jogo."
                if (serverMessage.equals("Fim do jogo.")) {
                    break;
                }

                // Exibe a mensagem recebida do servidor
                System.out.println(serverMessage);

                // Se a mensagem iniciar com "Sua vez", permite ao usuário fazer uma jogada
                if (serverMessage.startsWith("Sua vez")) {
                    System.out.print("Digite sua jogada (linha coluna): ");
                    String move = userInput.readLine();
                    out.println(move); // Envia a jogada ao servidor
                }
            }

            // Fecha o socket
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

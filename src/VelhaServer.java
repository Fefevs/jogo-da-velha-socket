import java.io.*;
import java.net.*;

public class VelhaServer {
    public static void main(String[] args) {
        char[][] board = new char[3][3]; // Cria um tabuleiro vazio 3x3
        boolean xTurn = true; // Inicialmente, é a vez do jogador X
        int moves = 0; // Conta o número de movimentos
        boolean gameEnded = false; // Indica se o jogo terminou

        try {
            ServerSocket serverSocket = new ServerSocket(12345); // Cria um servidor socket na porta 12345
            System.out.println("Aguardando a conexão do jogador...");

            Socket clientSocket = serverSocket.accept(); // Aguarda a conexão de um jogador
            System.out.println("Jogador conectado.");

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream())); // Para receber mensagens do jogador
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true); // Para enviar mensagens ao jogador

            out.println("Bem-vindo ao Jogo da Velha!");

            do {
                out.println(getBoardAsString(board)); // Envia o estado atual do tabuleiro ao jogador
                String currentPlayer = xTurn ? "X" : "O"; // Determina o jogador atual
                out.println("Sua vez, jogador " + currentPlayer + "!"); // Informa ao jogador que é a sua vez

                String move = in.readLine(); // Recebe a jogada do jogador
                if (isValidMove(move, board)) { // Verifica se a jogada é válida
                    int row = Character.getNumericValue(move.charAt(0));
                    int col = Character.getNumericValue(move.charAt(2));
                    board[row][col] = xTurn ? 'X' : 'O'; // Atualiza o tabuleiro com a jogada
                    xTurn = !xTurn; // Alterna o turno entre X e O
                    moves++;

                    if (checkWin(board, xTurn ? 'X' : 'O')) { // Verifica se alguém venceu o jogo
                        out.println(getBoardAsString(board)); // Envia o estado final do tabuleiro ao jogador
                        out.println("Jogador " + (xTurn ? "X" : "O") + " venceu! Parabéns!"); // Informa o vencedor
                        gameEnded = true;
                    } else if (moves == 9) { // Verifica se houve um empate
                        out.println(getBoardAsString(board));
                        out.println("O jogo terminou em empate.");
                        gameEnded = true;
                    }
                } else {
                    out.println("Movimento inválido. Tente novamente.");
                }

                out.println((!gameEnded) ?  "Fim do jogo." : " "); // caso a variavel seja verdadeira  imprime fim de jogo!
            } while (moves < 9 && !gameEnded); // Continua enquanto houver movimentos disponíveis e o jogo não terminou


            clientSocket.close();
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Função para verificar se um movimento é válido
    private static boolean isValidMove(String move, char[][] board) {
        if (move.length() != 3 || move.charAt(1) != ' ' || !Character.isDigit(move.charAt(0)) || !Character.isDigit(move.charAt(2))) {
            return false;
        }
        int row = Character.getNumericValue(move.charAt(0));
        int col = Character.getNumericValue(move.charAt(2));
        return row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '\0';
    }

    // Função para verificar se alguém venceu o jogo
    private static boolean checkWin(char[][] board, char player) {
        // Verificar linhas, colunas e diagonais para determinar a vitória
        for (int i = 0; i < 3; i++) {
            if ((board[i][0] == player && board[i][1] == player && board[i][2] == player) ||
                    (board[0][i] == player && board[1][i] == player && board[2][i] == player)) {
                return true;
            }
        }
        return (board[0][0] == player && board[1][1] == player && board[2][2] == player) ||
                (board[0][2] == player && board[1][1] == player && board[2][0] == player);
    }

    // Função para obter uma representação de string do tabuleiro
    private static String getBoardAsString(char[][] board) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                sb.append(board[i][j] != '\0' ? board[i][j] : '-');
                if (j < 2) {
                    sb.append(" | ");
                }
            }
            sb.append("\n");
            if (i < 2) {
                sb.append("---------\n");
            }
        }
        return sb.toString();
    }
}
